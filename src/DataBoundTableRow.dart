part of DataBoundTable;

class DataBoundTableRow {

	final DataBoundTable parent;
	var _ID;
	var _json;

	var Items;

	var _dirty = true;
	var _row = null;

	DataBoundTableRow(this.parent, this._json) {
		_parse();
	}

	_parse() {
		Items = new List<DataBoundTableRowItem>();
		_json.forEach((k, v) => Items.add(new DataBoundTableRowItem(this, k, value:v)));
	}

	draw() {
		_dirty = false;
		var cols = parent.Columns.where((x) => x.Visible).toList();

		if(_row == null)
			_row = parent._table.addRow();
		else
			_row.children.clear();

		for (var col in cols) {
			for (var item in Items) {
				if (item.column == col.ID) {
					var cell = _row.addCell().text = item.value.toString();
					break;
				}
			}
		}
	}

	dirtifiy() {
		if(parent.AutoRedraw && parent._table != null)
			draw();
		else if(parent._table != null) {
			_dirty = true;
			parent._dirtify();
		}
	}

	getID() { return _ID; }
	isDirty() { return _dirty; }
}