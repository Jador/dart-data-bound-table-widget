library DataBoundTable;

import 'dart:html';
import 'dart:async';
import 'dart:json' as JSON;

part 'DataBoundTableRow.dart';
part 'DataBoundTableRowItem.dart';
part 'DataBoundTableColumn.dart';

class DataBoundTable {

	final ID;
	var json;
	var parent;
	var _table = null;
	var _dirty = false;
	var _header = null;

	var Columns;
	var Rows;

	var ShowHeaderRow = true;
	var AutoRedraw = true;

	var _controller;
	var onDirty;

	DataBoundTable(String json, this.parent, this.ID, [bool draw = true]) {
		_controller  = new StreamController();
		onDirty = _controller.stream;

		this.json = JSON.parse(json);
		_parse();

		if (draw != null && draw)
			this.draw();
	}

	draw() {
		if (_table == null) {
			_table = new TableElement();
			_table.id = ID;
			query('#' + parent).children.add(_table);
		}

		var columnDirty = false;

		for(DataBoundTableColumn column in Columns) {
			if(column.isDirty()) {
				columnDirty = true;
				break;
			}
		}

		if(ShowHeaderRow && columnDirty)
			drawHeader();

		Rows.where((x) => x.isDirty()).toList().forEach((n) => n.draw());

		_dirty = false;
	}

	drawHeader() {
		var sb = new StringBuffer();
		Columns.where((x) => x.Visible).toList().forEach((n) {
			n._dirty = false;
			sb.write('<th>' + n.Name + '</th>');
		});

		if(_header == null) {
			var el = new Element.html('<thead class="headerstyle"></thead>');
			_header = new TableRowElement();

			el.children.add(_header);
			_table.children.add(el);
		}
		else
			_header.children.clear();

		_header.appendHtml(sb.toString());
	}

	_parse() {
		Columns = new List<DataBoundTableColumn>();
		json[0].keys.forEach((n) => Columns.add(new DataBoundTableColumn(this, n)));

		Rows = new List<DataBoundTableRow>();
		json.forEach((x) => Rows.add(new DataBoundTableRow(this, x)));
	}

	_dirtify() {
		_controller.add('$_table is dirty');
		_dirty = true;
	}
}