part of DataBoundTable;

class DataBoundTableColumn {
	final ID;
  final DataBoundTable parent;

	var _name;
	var _dirty = true;

	var Visible = true;

	get Name => _name;
	set Name(name) {
		_name = name;
		if(parent.AutoRedraw && parent._table != null)
			parent.drawHeader();
		else if(parent._table != null) {
			_dirty = true;
			parent._dirtify();
		}
	}

	DataBoundTableColumn(this.parent, this.ID, {String name}) {
		if (?name) {
			_name = name;
		}
		else {
			_name = ID;
		}
	}

  isDirty() { return _dirty; }
}