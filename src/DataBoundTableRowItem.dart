part of DataBoundTable;

class DataBoundTableRowItem {
	final column;
	final DataBoundTableRow parent;

	var _value;

	get value => _value;
	set value(x) {
		_value = x;
		parent.dirtifiy();
	}

	DataBoundTableRowItem(this.parent, this.column, {value}) {
		if (?value && value != null) {
			this.value = value;
		}
		else {
			this.value = "";
		}
	}
}